package com.example.bank.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.bank.service.InfoSaldoService;

@RestController
@RequestMapping("/infosaldo")
public class infoSaldoController {
	@Autowired
	InfoSaldoService infoSaldoService;
	
	@GetMapping("gruprekening/view-gruprekening-by-nama")
	 public ResponseEntity<Object> getGrupRekening(){
		return new ResponseEntity<>(infoSaldoService.viewGrupRekeningDto(),HttpStatus.OK);
	}
	@GetMapping("rekening/view-rekening-by-gruprekening-and-tiperekening")
	public ResponseEntity<Object> getRekening(){
		return new ResponseEntity<>(infoSaldoService.viewRekeningDto(),HttpStatus.OK);
	}
	@GetMapping("tiperekening/view-tiperekening-by-nama")
	public ResponseEntity<Object> getTipeRekening(){
		return new ResponseEntity<>(infoSaldoService.viewTipeRekeningDto(),HttpStatus.OK);
	}
}
