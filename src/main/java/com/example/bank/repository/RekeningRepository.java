package com.example.bank.repository;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.bank.entity.Rekening;

@Repository
public interface RekeningRepository extends PagingAndSortingRepository<Rekening,String>{
	public List<Rekening> findAll();
}
