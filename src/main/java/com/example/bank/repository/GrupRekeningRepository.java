package com.example.bank.repository;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.bank.entity.GrupRekening;

@Repository
public interface GrupRekeningRepository extends PagingAndSortingRepository<GrupRekening,String> {
	public List<GrupRekening> findAll();
}
