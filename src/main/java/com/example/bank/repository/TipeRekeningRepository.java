package com.example.bank.repository;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.bank.entity.TipeRekening;

@Repository
public interface TipeRekeningRepository extends PagingAndSortingRepository<TipeRekening,String> {
	public List<TipeRekening> findAll();
}
