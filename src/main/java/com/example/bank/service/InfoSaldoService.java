package com.example.bank.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bank.dto.GrupRekeningDto;
import com.example.bank.dto.RekeningDto;
import com.example.bank.dto.TipeRekeningDto;
import com.example.bank.entity.GrupRekening;
import com.example.bank.entity.Rekening;
import com.example.bank.entity.TipeRekening;
import com.example.bank.repository.GrupRekeningRepository;
import com.example.bank.repository.RekeningRepository;
import com.example.bank.repository.TipeRekeningRepository;

@Service
public class InfoSaldoService {
	@Autowired
	GrupRekeningRepository grupRekeningRepository;
	@Autowired
	TipeRekeningRepository tipeRekeningRepository;
	@Autowired
	RekeningRepository rekeningRepository;
	public List<GrupRekeningDto> viewGrupRekeningDto() {
		// TODO Auto-generated method stub	
		List<GrupRekeningDto> rto = new ArrayList();
		List<GrupRekening> r = grupRekeningRepository.findAll();
		for (int i = 0; i < r.size(); i++) {
			GrupRekeningDto dto = new GrupRekeningDto();
			dto.setNama(r.get(i).getNama());
			rto.add(dto);
		}
		return rto; 
	}
	public List<RekeningDto> viewRekeningDto() {
		// TODO Auto-generated method stub	
		List<RekeningDto> rto = new ArrayList();
		List<Rekening> r = rekeningRepository.findAll();
		for (int i = 0; i < r.size(); i++) {
			RekeningDto dto = new RekeningDto();
			dto.setNama(r.get(i).getNama());
			dto.setNomor(r.get(i).getNomor());
			dto.setSaldo(r.get(i).getSaldo());
			dto.setTiperekening(r.get(i).getTiperekening());
			dto.setNamacabang(r.get(i).getNamacabang());
			dto.setPlatfon(r.get(i).getPlatfon());
			dto.setJumlahblokir(r.get(i).getJumlahblokir());
			dto.setSetorbelumefektif(r.get(i).getSetorbelumefektif());
			rto.add(dto);
		}
		return rto; 
	}
	public List<TipeRekeningDto> viewTipeRekeningDto(){
		List<TipeRekeningDto> tro = new ArrayList();
		List<TipeRekening> tr = tipeRekeningRepository.findAll();
		for (int i = 0; i < tr.size(); i++) {
			TipeRekeningDto dto = new TipeRekeningDto();
			dto.setNama(tr.get(i).getNama());
			tro.add(dto);
		}
		return tro; 
	}
}
