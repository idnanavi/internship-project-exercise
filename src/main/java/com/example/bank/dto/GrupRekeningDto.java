package com.example.bank.dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class GrupRekeningDto {
	private String nama;
}
