package com.example.bank.dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class RekeningDto {

	private String nomor;
	private String tiperekening;
	private String nama;
	private int saldo;
	private String namacabang;
	private int platfon;
	private int jumlahblokir;
	private int setorbelumefektif;
}
