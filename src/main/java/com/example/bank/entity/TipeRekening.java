package com.example.bank.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tipe_rekening")
@Getter @Setter @NoArgsConstructor
public class TipeRekening {
	@Id
	@GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid",strategy="uuid")
	@Column(name="id_tiperekening")
	private String id;
	@Column(name="kode_tiperekening")
	private String kode;
	@Column(name="kode_gruptiperekening")
	private String kode_grup;
	@Column(name="nama_tiperekening")
	private String nama;
	@Column(name="created_date")
	private Date created;
	@Column(name="updated_date")
	private Date updated;
}
