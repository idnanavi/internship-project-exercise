package com.example.bank.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="rekening")
@Getter @Setter @NoArgsConstructor
public class Rekening {
	@Id
	@GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid",strategy="uuid")
	@Column(name="id_rekening")
	private String id;
	@Column(name="nomor_rekening")
	private String nomor;
	@Column(name="tipe_rekening")
	private String tiperekening;
	@Column(name="nama_rekening")
	private String nama;
	@Column(name="alias_rekening")
	private String alias;
	@Column(name="status_rekening")
	private String status;
	@Column(name="saldo_rekening")
	private int saldo;
	@Column(name="kodecabang_rekening")
	private String kodecabang;
	@Column(name="namacabang_rekening")
	private String namacabang;
	@Column(name="platfon_rekening")
	private int platfon;
	@Column(name="jumlahblokir_rekening")
	private int jumlahblokir;
	@Column(name="setorbelumefektif_rekening")
	private int setorbelumefektif;
	@Column(name="isinquiry_rekening")
	private String isinquiry;
	@Column(name="isdebit_rekening")
	private String isdebit;
	@Column(name="iscredit_rekening")
	private String iscredit;
	@Column(name="created_date")
	private Date created;
	@Column(name="updated_date")
	private Date updated;
}
